* https://ukpowernetworks.opendatasoft.com/explore/?disjunctive.theme&disjunctive.keyword&sort=explore.popularity_score
* https://connecteddata.nationalgrid.co.uk/dataset/
* https://usmart.io/org/esc/
* https://data.london.gov.uk/
* https://ukerc.rl.ac.uk/
* https://es.catapult.org.uk/tools-and-labs/open-data/
* https://github.com/OSUKED
* https://data.ssen.co.uk/@ssen-distribution/ssen_smart_meter_prod_lv_feeder
